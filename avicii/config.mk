PRODUCT_SOONG_NAMESPACES += \
    device/oneplus/apps/avicii

PRODUCT_COPY_FILES += \
    device/oneplus/apps/avicii/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    device/oneplus/apps/avicii/proprietary/system/etc/sysconfig/hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist.xml \
    device/oneplus/apps/avicii/proprietary/system_ext/etc/permissions/privapp-permissions-oem-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oem-system_ext.xml \
    device/oneplus/apps/avicii/proprietary/system_ext/etc/sysconfig/hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/hiddenapi-package-whitelist.xml \
    device/oneplus/apps/avicii/proprietary/system_ext/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so

PRODUCT_PACKAGES += \
    OnePlusCameraService \
    OnePlusCamera \
    OnePlusGallery
