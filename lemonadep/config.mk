PRODUCT_SOONG_NAMESPACES += \
    device/oneplus/apps/lemonadep

PRODUCT_COPY_FILES += \
    device/oneplus/apps/lemonadep/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    device/oneplus/apps/lemonadep/proprietary/system_ext/etc/permissions/com.oneplus.camera.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oneplus.camera.xml \
    device/oneplus/apps/lemonadep/proprietary/system_ext/etc/configs/hiddenapi-package-whitelist-oneplus.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml \
    device/oneplus/apps/lemonadep/proprietary/system_ext/etc/permissions/com.oneplus.gallery.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oneplus.gallery.xml

PRODUCT_PACKAGES += \
    GoogleCamera \
    OnePlusGallery
