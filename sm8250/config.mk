PRODUCT_SOONG_NAMESPACES += \
    device/oneplus/apps/sm8250

PRODUCT_COPY_FILES += \
    device/oneplus/apps/sm8250/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    device/oneplus/apps/sm8250/proprietary/system/etc/sysconfig/hiddenapi-package-whitelist-oem.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-oem.xml \
    device/oneplus/apps/sm8250/proprietary/system_ext/etc/permissions/com.oneplus.camera.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oneplus.camera.xml \
    device/oneplus/apps/sm8250/proprietary/system_ext/etc/permissions/privapp-permissions-oem-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oem-system_ext.xml \
    device/oneplus/apps/sm8250/proprietary/system_ext/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so

PRODUCT_PACKAGES += \
    CameraPicProcService \
    OnePlusCameraService \
    OnePlusCamera \
    OnePlusGallery
